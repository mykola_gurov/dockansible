build:
	docker build -t local/ansible .
run: build
	docker run --rm -it -v $(shell pwd)/:/work:ro -w /work local/ansible /bin/bash
sshd:
	/usr/sbin/sshd -D &
play:
	ansible-playbook some.yml